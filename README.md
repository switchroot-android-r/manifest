# Switch Manifest for LineageOS 18.1 (Android R)

### Disclaimer
Switchroot Android R is NOT SUPPORTED. This product is in an ALPHA STATE and delivered as-is, with no support or assistance available at this time. You may (will) encounter instability and bugs, including but not limited to the issues listed below.

### Platforms
* `icosa_sr`: Mobile Android with Nvidia Games app and other enhancements
* `icosa_tv_sr`: Android TV with touch capability based on ATV for Nvidia Shield

### Issues
* Volume UI broken
* Nvidia enhancements incomplete
* HWC glitches
* Q glitches (see https://gitlab.com/switchroot/android/manifest)

### Patching
Basic:
* Pick topic `nvidia-enhancements-r`, `nvidia-shieldtech-r`, `icosa-bt-r`, `nvidia-nvgpu-r`, `304329`, and `302555` off Lineage Gerrit
* Apply all patches in `patches/` except hwc patches
* If you want software rendering forced on (graphical glitches and marginally degraded performance), apply `device_nintendo_icosa_sr-hwc.patch`
* If you want hardware overlays enabled (half-screen on boot, no dock support), apply `device_nintendo_icosa_sr-bring-back-hwc.patch`

Overclocking:
* Apply all patches in `oc/` for a 2091 MHz overclock
